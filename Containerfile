# @REF [Building elixr release with docker](https://blog.miguelcoba.com/deploying-a-phoenix-16-app-with-docker-and-elixir-releases)

FROM quay.io/reavessm/elixir-cargo as builder

#RUN apk add --no-cache build-base git python3 curl cargo
#RUN dnf install -y elixir cargo && dnf clean all -y

WORKDIR /app

ENV MIX_ENV="prod"
ENV ELIXIR_ERL_OPTIONS="+fnu"

RUN mix local.hex --force \
  && mix local.rebar --force

RUN mkdir config

COPY mix.exs mix.lock ./
COPY config/config.exs config/prod.exs config/runtime.exs config/
COPY lib lib
COPY native/grdr_builder/src native/grdr_builder/src
COPY native/grdr_builder/Cargo.toml native/grdr_builder/Cargo.toml
COPY native/grdr_builder/README.md native/grdr_builder/README.md
COPY priv/static priv/static
COPY priv/gettext priv/gettext
COPY assets assets

RUN mix deps.get \
  && mix deps.compile \
  && mix assets.deploy \
  && mix compile \
  && mix release

FROM quay.io/reavessm/fedora-utils:latest as app

ARG MIX_ENV

#RUN apk add --no-cache libstdc++ openssl ncurses-libs graphviz
#RUN dnf install -y elixir graphviz
RUN dnf install -y graphviz openssl && dnf clean all -y

ENV MIX_ENV="prod"
ENV SECRET_KEY_BASE=""
ENV ELIXIR_ERL_OPTIONS="+fnu"

ENV USER="elixir"

# Create  unprivileged user to run the release
RUN useradd -m elixir

WORKDIR "/home/${USER}/app"

# run as user
USER ${USER}

# copy release executables
COPY --from=builder --chown="${USER}":"${USER}" /app/_build/prod/rel/grdr_beam ./

ENTRYPOINT ["/bin/bash", "-c"]

CMD ["bin/grdr_beam start"]

EXPOSE 4000

VOLUME content
