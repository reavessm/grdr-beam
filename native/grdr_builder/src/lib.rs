use fast_qr::{
    convert::{svg::SvgBuilder, Builder, Shape},
    QRBuilder,
};
use lazy_static::lazy_static;

// Examples showing how to add custom rivets
lazy_static! {
    static ref RIVET_LIST: grdr::rivets::RivetList = grdr::rivets::RivetList::default()
        .add_rivet(("embed", embed_rivet()))
        .add_rivet(("quote", quote_rivet()))
        .add_rivet(("define", define_rivet()))
        .add_rivet(("qr", qr_rivet()))
        .add_rivet(("toot", toot_rivet()))
        .add_rivet(("tikz", tikz_rivet()))
        // You can add rivets with inline rivets
        .add_rivet(
            (
                "foo",
                |_file_name: &str, _input: &str| {
                        return "FOO".to_owned();
                }
            )
        )
        // You can add multiple rivets at once
        .add_rivets(vec![
            // Calling a function that returns rivet will make the signature
            // explicit, while making this call a touch inconvenient
            ("bar", bar()),
            // You can, of course, just call a rivet directly
            ("baz", baz)
        ]);
}

// TODO: Change to https://github.com/tectonic-typesetting/tectonic/blob/master/Cargo.toml ?
fn tikz_rivet() -> grdr::rivets::Rivet {
    |_file_name: &str, input: &str| {
        return format!(
            r#"<div class="svg-wrapper">
            <script type="text/tikz">{}</script>
            </div>"#,
            input
        );
    }
}

fn toot_rivet() -> grdr::rivets::Rivet {
    |_file_name: &str, input: &str| {
        // r#"https://fosstodon.org/@rapscallionreaves/112534965995480162"#
        let tokens: Vec<&str> = input.split('/').filter(|s| !s.is_empty()).collect();

        if tokens.len() != 4 {
            return format!("Error! Invalid URL! {:?}", tokens);
        }

        return format!(
            r#"<h2 id="Comments from Mastodon"><a href="{}">Comments from Mastdon</a></h2>
            <a class="mastodon-thread"
               href="{}//{}/{}"
               data-exclude-post=true
               data-toot-id="{}"
               >Thread from the Fediverse</a>"#,
            input,
            tokens.get(0).unwrap(),
            tokens.get(1).unwrap(),
            tokens.get(2).unwrap(),
            tokens.get(3).unwrap()
        );
    }
}

fn embed_rivet() -> grdr::rivets::Rivet {
    |_file_name: &str, input: &str| {
        // TODO: Handle multiple 'type's
        return format!(
            r#"<br><embed src="{}" width="100%" height="375" type="application/pdf"><br>"#,
            input.trim()
        )
        .to_owned();
    }
}

// TODO: Add ability to link directly to definition
fn define_rivet() -> grdr::rivets::Rivet {
    |_file_name: &str, input: &str| {
        if input.trim() == "" {
            return String::new();
        }

        let mut lines = input.lines();

        let (definition, headword) = if lines.clone().count() == 1 {
            (lines.next().unwrap().trim().to_owned(), String::new())
        } else {
            let h = lines.next().unwrap().trim().to_owned();
            let d: String = lines.collect::<Vec<&str>>().join(" ");
            (d, h)
        };

        return format!(
            "<blockquote><cite>{}</cite><br>{}</blockquote>",
            headword, definition
        )
        .to_owned();
    }
}

fn quote_rivet() -> grdr::rivets::Rivet {
    |_file_name: &str, input: &str| {
        if input.trim() == "" {
            return String::new();
        }

        let mut lines = input.lines();

        let (quote, author) = if lines.clone().count() == 1 {
            (lines.next().unwrap().trim().to_owned(), String::new())
        } else {
            let a = lines.next().unwrap().trim().to_owned();
            let q: String = lines.collect::<Vec<&str>>().join(" ");
            (q, a)
        };

        return format!(
            "<blockquote>{}<br><cite>{}</cite></blockquote>",
            quote, author
        )
        .to_owned();
    }
}

fn qr_rivet() -> grdr::rivets::Rivet {
    |_file_name: &str, input: &str| {
        if input.trim() == "" {
            return String::new();
        }

        let qrcode = QRBuilder::new(input.trim())
            .ecl(fast_qr::ECL::H)
            .build()
            .unwrap();
        return format!(
            r#"<div id="qr">{}</div>"#,
            SvgBuilder::default()
                .image_size(15.0)
                .shape(Shape::Square)
                .to_str(&qrcode)
        );
    }
}

fn bar() -> grdr::rivets::Rivet {
    |_file_name: &str, _input: &str| return "BAR".to_owned()
}

// Enforce baz to be a Rivet
const _: grdr::rivets::Rivet = baz;
fn baz(_file_name: &str, _input: &str) -> String {
    return "BAZ".to_owned();
}

#[rustler::nif]
fn add(a: i64, b: i64) -> i64 {
    a + b
}

#[rustler::nif(schedule = "DirtyCpu")]
fn build(filename: String) -> Option<String> {
    println!("Building {filename}");
    match serde_json::to_string(&grdr::types::Post::build_from(
        &filename,
        RIVET_LIST.clone(),
    )) {
        Ok(s) => return Some(s),
        Err(why) => {
            eprintln!("Errror: {why}");
            return None;
        }
    }
}

#[rustler::nif(schedule = "DirtyCpu")]
fn get_meta(filename: String) -> Option<String> {
    let (meta, _) = grdr::types::Metadata::split(std::fs::read_to_string(filename).unwrap());
    match serde_json::to_string(&meta) {
        Ok(s) => return Some(s),
        Err(why) => {
            eprintln!("Errror: {why}");
            return None;
        }
    }
}

#[rustler::nif]
fn oops() -> String {
    grdr::renderers::render_oops().into()
}

#[rustler::nif]
fn list_posts() -> String {
    println!("Listing posts");

    let mut p = String::new();

    let mut posts = grdr::types::PostList::new();
    posts.list(String::from("./content/posts"));

    match serde_json::to_string(&posts.posts) {
        Ok(s) => p.push_str(&s),
        Err(why) => {
            eprintln!("Errror: {why}");
        }
    }

    return p;
}

#[rustler::nif]
fn list_notes() -> String {
    println!("Listing posts");

    let mut p = String::new();

    let mut posts = grdr::types::PostList::new();
    posts.list(String::from("./content/notes"));

    match serde_json::to_string(&posts.posts) {
        Ok(s) => p.push_str(&s),
        Err(why) => {
            eprintln!("Errror: {why}");
        }
    }

    return p;
}

#[rustler::nif]
fn list_talks() -> String {
    println!("Listing talks");

    let mut t = String::new();

    let mut talks = grdr::types::PostList::new();
    talks.list(String::from("./content/talks"));

    match serde_json::to_string(&talks.posts) {
        Ok(s) => t.push_str(&s),
        Err(why) => {
            eprintln!("Error: {why}");
        }
    }

    return t;
}

rustler::init!("Elixir.GrdrBeam");
