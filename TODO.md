# TODO

- [x] Implement build cache using [ETS](https://elixirschool.com/en/lessons/storage/ets)
- [x] Tags
- [ ] Add LiveView
  - [ ] Slides, similar to [a gallery app](https://www.poeticoding.com/understanding-liveview-build-a-gallery-app/)
    - LiveView
    - GRDR needs to return (Metadata, List<"<article>foo</article>">)
  - [ ] Suspense/Fallback thingy so pages don't take so long to return something
- [x] New top-level directory called Notes that's just my OMSCS stuff
  - This means top-level should probably be dynamic
- [ ] Swap ets cache to cachex to add time-based invalidation
