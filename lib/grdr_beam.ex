defmodule GrdrBeam do
  @moduledoc """
  GrdrBeam keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """
  use Rustler,
    # must match the name of the project in `mix.exs`
    # otp_app: :grdr_beam,
    otp_app: :grdr_beam,
    # must match the name of the crate in `native/<foo>/Cargo.toml`
    crate: :grdr_builder

  # All functions in rust that are init'ed need to be here
  def add(_arg1, _arg2), do: :erlang.nif_error(:nif_not_loaded)
  def build(_filename), do: :erlang.nif_error(:nif_not_loaded)
  def get_meta(_filename), do: :erlang.nif_error(:nif_not_loaded)
  def gen(), do: :erlang.nif_error(:nif_not_loaded)
  def list_posts(), do: :erlang.nif_error(:nif_not_loaded)
  def list_notes(), do: :erlang.nif_error(:nif_not_loaded)
  def list_talks(), do: :erlang.nif_error(:nif_not_loaded)
  def oops(), do: :erlang.nif_error(:nif_not_loaded)

  def file_list(prefix) do
    # prefix = "content/posts/"

    Path.wildcard("./" <> prefix <> "*.md")
    # Remove prefix and filetype from filename
    |> Enum.map(fn str -> str |> String.slice(String.length(prefix)..-4) end)
  end

  def top_level_list() do
    file_list("content/topLevel")
  end

  def post_list() do
    # file_list("content/posts/")
    list_posts() |> Jason.decode() |> elem(1)
  end

  def note_list() do
    # file_list("content/notes/")
    list_notes() |> Jason.decode() |> elem(1)
  end

  def talk_list() do
    list_talks() |> Jason.decode() |> elem(1)
  end

  def build_post() do
    files =
      for {v, k} <- post_list() |> Enum.with_index(),
          into: %{},
          do: {(k + 1) |> Integer.to_string(), v}

    IO.inspect(files)

    files[IO.gets("Please choose a file: ") |> String.trim()] |> build()
  end

  def build_file(name) do
    System.untrap_signal(:sigchld, :dirty)

    {:ok, :dirty} =
      System.trap_signal(:sigchld, :dirty, fn ->
        :ok
      end)

    content = name |> build |> Jason.decode() |> elem(1)

    System.untrap_signal(:sigchld, :dirty)

    content
  end
end
