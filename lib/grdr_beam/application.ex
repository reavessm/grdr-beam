defmodule GrdrBeam.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    GrdrBeamWeb.init_cache()

    children = [
      # Start the Telemetry supervisor
      GrdrBeamWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: GrdrBeam.PubSub},
      # Start the Endpoint (http/https)
      GrdrBeamWeb.Endpoint
      # Start a worker by calling: GrdrBeam.Worker.start_link(arg)
      # {GrdrBeam.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: GrdrBeam.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    GrdrBeamWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
