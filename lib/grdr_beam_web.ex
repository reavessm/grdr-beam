defmodule GrdrBeamWeb do
  @file_lookup :file_lookup
  @note_lookup :note_lookup
  @talk_lookup :talk_lookup
  @tag_lookup :tag_lookup
  @file_cache :file_cache

  @moduledoc """
  The entrypoint for defining your web interface, such
  as controllers, components, channels, and so on.

  This can be used in your application as:

      use GrdrBeamWeb, :controller
      use GrdrBeamWeb, :html

  The definitions below will be executed for every controller,
  component, etc, so keep them short and clean, focused
  on imports, uses and aliases.

  Do NOT define functions inside the quoted expressions
  below. Instead, define additional modules and import
  those modules here.
  """

  def static_paths, do: ~w(assets fonts images favicon.ico robots.txt vendor)

  def router do
    quote do
      use Phoenix.Router, helpers: false

      # Import common connection and controller functions to use in pipelines
      import Plug.Conn
      import Phoenix.Controller
      import Phoenix.LiveView.Router
    end
  end

  def channel do
    quote do
      use Phoenix.Channel
    end
  end

  def controller do
    quote do
      use Phoenix.Controller,
        formats: [:html, :json],
        layouts: [html: GrdrBeamWeb.Layouts]

      import Plug.Conn
      import GrdrBeamWeb.Gettext

      unquote(verified_routes())
    end
  end

  def live_view do
    quote do
      use Phoenix.LiveView,
        layout: {GrdrBeamWeb.Layouts, :app}

      unquote(html_helpers())
    end
  end

  def live_component do
    quote do
      use Phoenix.LiveComponent

      unquote(html_helpers())
    end
  end

  def html do
    quote do
      use Phoenix.Component

      # Import convenience functions from controllers
      import Phoenix.Controller,
        only: [get_csrf_token: 0, view_module: 1, view_template: 1]

      # Include general helpers for rendering HTML
      unquote(html_helpers())
    end
  end

  defp html_helpers do
    quote do
      # HTML escaping functionality
      import Phoenix.HTML
      # Core UI components and translation
      import GrdrBeamWeb.CoreComponents
      import GrdrBeamWeb.Gettext

      # Shortcut for generating JS commands
      alias Phoenix.LiveView.JS

      # Routes generation with the ~p sigil
      unquote(verified_routes())
    end
  end

  def verified_routes do
    quote do
      use Phoenix.VerifiedRoutes,
        endpoint: GrdrBeamWeb.Endpoint,
        router: GrdrBeamWeb.Router,
        statics: GrdrBeamWeb.static_paths()
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end

  # @REF [ETS](https://codereview.stackexchange.com/questions/214140/storing-temporary-data-in-ets-for-example-using-ets-in-a-web-wizard)
  def init_cache() do
    (IO.ANSI.color(1, 1, 1) <> "Initializing cache" <> IO.ANSI.reset()) |> IO.puts()

    init_file_cache()
    init_file_lookup()
    init_note_lookup()
    init_talk_lookup()
    init_tag_lookup()

    Path.wildcard("content/**/*.md")
    |> Enum.each(fn file_name ->
      meta =
        GrdrBeam.get_meta(file_name)
        |> Jason.decode()
        |> elem(1)

      add_to_tag_lookup(
        {Path.basename(file_name), meta},
        meta
        |> Map.get("tags")
      )

      cond do
        Path.dirname(file_name) |> String.starts_with?("content/notes") ->
          add_to_note_lookup(Path.basename(file_name), file_name)

        Path.dirname(file_name) |> String.starts_with?("content/talks") ->
          add_to_talk_lookup(Path.basename(file_name), file_name)

        true ->
          add_to_lookup(Path.basename(file_name), file_name)
      end
    end)

    # Warm cache
    for {file, path} <- [
          {"showcase", "content/topLevel/showcase.md"},
          {"about", "content/topLevel/about.md"}
        ] do
      IO.ANSI.color(1, 1, 1) |> IO.write()

      if File.exists?(path) do
        file
        |> GrdrBeamWeb.put(
          GrdrBeam.build_file(path)
          |> Map.get("content")
        )
      end

      IO.ANSI.reset() |> IO.write()
    end
  end

  def list_files() do
    :ets.tab2list(@file_lookup)
  end

  def list_notes() do
    :ets.tab2list(@note_lookup)
  end

  def list_talks() do
    :ets.tab2list(@talk_lookup)
  end

  defp init_cache(name, write_concurreny \\ false) do
    :ets.new(name, [
      :public,
      :named_table,
      read_concurrency: true,
      write_concurrency: write_concurreny
    ])
  end

  defp init_file_cache() do
    init_cache(@file_cache, true)
  end

  defp init_file_lookup() do
    init_cache(@file_lookup)
  end

  defp init_note_lookup() do
    init_cache(@note_lookup)
  end

  defp init_talk_lookup() do
    init_cache(@talk_lookup)
  end

  defp init_tag_lookup() do
    init_cache(@tag_lookup)
  end

  def lookup(filename, table \\ @file_lookup) do
    case :ets.lookup(table, String.downcase(filename)) do
      [{_, filepath}] -> {:ok, filepath}
      _ -> {:not_found}
    end
  end

  def note_lookup(filename) do
    lookup(filename, @note_lookup)
  end

  def talk_lookup(filename) do
    lookup(filename, @talk_lookup)
  end

  def tag_lookup(tag) do
    case :ets.lookup(@tag_lookup, String.downcase(tag)) do
      [{_, file_list}] -> file_list
      _ -> []
    end
  end

  def add_to_lookup(filename, filepath) do
    :ets.insert(@file_lookup, {String.downcase(filename), filepath})
  end

  def add_to_note_lookup(filename, filepath) do
    :ets.insert(@note_lookup, {String.downcase(filename), filepath})
  end

  def add_to_talk_lookup(filename, filepath) do
    :ets.insert(@talk_lookup, {String.downcase(filename), filepath})
  end

  def add_to_tag_lookup({filename, meta}, tag_list) do
    tag_list
    |> Enum.map(&String.downcase/1)
    |> Enum.map(fn t ->
      {t,
       [{filename, meta} | tag_lookup(t)]
       |> Enum.sort()
       |> Enum.uniq()}
    end)
    # Destructuring is unnecessary here, but better to be explicit on what is
    # returned from above.  Hopefully compiler will optimize.
    |> Enum.each(fn {tag, file_list} ->
      :ets.insert(@tag_lookup, {tag, file_list})
    end)
  end

  def get(filepath) do
    case :ets.lookup(@file_cache, filepath) do
      [{^filepath, content}] -> {:ok, content}
      _ -> {:not_found}
    end
  end

  def put(filepath, content) do
    :ets.insert(@file_cache, {filepath, content})
  end

  def build_or_get(
        build_fn,
        %{name: name, cache: cache}
      ) do
    IO.ANSI.color(1, 1, 1) |> IO.write()

    result =
      if cache == nil || cache == "true" do
        case GrdrBeamWeb.get(name) do
          {:ok, content} ->
            IO.puts("Cached")
            content

          _ ->
            IO.puts("Built")

            content = build_fn.(name)

            GrdrBeamWeb.put(name, content)

            content
        end
      else
        content = build_fn.(name)

        GrdrBeamWeb.put(name, content)

        content
      end

    IO.ANSI.reset() |> IO.write()

    result
  end

  def drop(), do: :ets.delete_all_objects(@file_cache)
  def delete(filepath), do: :ets.delete(@file_cache, filepath)
  # defp delete(filepath), do: :ets.delete(@file_cache, filepath)
end
