defmodule GrdrBeamWeb.Router do
  use GrdrBeamWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, html: {GrdrBeamWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", GrdrBeamWeb do
    pipe_through :browser

    get "/", PageController, :home

    get "/posts", PostController, :index

    get "/posts/tags", PostController, :index
    get "/posts/tags/:tag", PostController, :tags

    get "/posts/:name", PostController, :post

    get "/notes", PostController, :notes
    get "/notes/:name", PostController, :note

    get "/about", TopLevelController, :about
    get "/showcase", TopLevelController, :showcase

    get "/imgs/:name", ImgController, :get
    get "/pdfs/:name", PdfController, :get
    get "/resume/:name", PdfController, :resume

    get "/talks", PostController, :slides
    get "/talks/:name", PostController, :slide
  end

  # Other scopes may use custom stacks.
  # scope "/api", GrdrBeamWeb do
  #   pipe_through :api
  # end

  # Enable LiveDashboard in development
  if Application.compile_env(:grdr_beam, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: GrdrBeamWeb.Telemetry
    end
  end
end
