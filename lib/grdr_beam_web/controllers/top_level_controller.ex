defmodule GrdrBeamWeb.TopLevelController do
  use GrdrBeamWeb, :controller

  # def index(conn, _params) do
  #   # If content dir does not exist
  #   # GrdrBeam.gen()
  #
  #   conn
  #   |> put_layout(html: :app)
  #   |> render(:posts, files: GrdrBeam.file_list())
  # end

  defp show(conn, %{name: name, template: template, cache: cache}) do
    conn
    |> put_layout(html: :app)
    |> assign(:name, name |> String.capitalize())
    |> render(template,
      content:
        GrdrBeamWeb.build_or_get(
          &(GrdrBeam.build_file("content/topLevel/" <> &1 <> ".md") |> Map.get("content")),
          %{name: name, cache: cache}
        )
    )
  end

  def about(conn, params) do
    show(conn, %{:name => "about", :template => :about, :cache => params["cache"]})
  end

  def showcase(conn, params) do
    show(conn, %{:name => "showcase", :template => :showcase, :cache => params["cache"]})
  end
end
