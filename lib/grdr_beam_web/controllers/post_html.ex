defmodule GrdrBeamWeb.PostHTML do
  use GrdrBeamWeb, :html

  embed_templates("post_html/*")
end
