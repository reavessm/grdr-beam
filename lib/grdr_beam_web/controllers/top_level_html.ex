defmodule GrdrBeamWeb.TopLevelHTML do
  use GrdrBeamWeb, :html

  embed_templates("top_level_html/*")
end
