defmodule GrdrBeamWeb.PageController do
  use GrdrBeamWeb, :controller

  def home(conn, _params) do
    # The home page is often custom made,
    # so skip the default app layout.

    # IO.puts("INSIDE HOME")
    render(conn, :home, layout: false, page_title: "reaves.dev")
  end
end
