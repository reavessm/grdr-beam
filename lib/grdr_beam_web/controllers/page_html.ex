defmodule GrdrBeamWeb.PageHTML do
  use GrdrBeamWeb, :html

  embed_templates "page_html/*"
end
