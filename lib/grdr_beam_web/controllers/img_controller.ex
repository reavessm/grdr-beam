defmodule GrdrBeamWeb.ImgController do
  use GrdrBeamWeb, :controller

  def get(conn, params) do
    IO.puts(params["name"])
    file_name = "content/static/imgs/" <> params["name"]

    case File.exists?(file_name) do
      true ->
        send_file(conn, 200, file_name)

      false ->
        send_resp(conn, 404, "Image not found")
    end
  end
end
