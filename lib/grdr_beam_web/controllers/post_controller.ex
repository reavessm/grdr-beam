defmodule GrdrBeamWeb.PostController do
  use GrdrBeamWeb, :controller

  def index(conn, params) do
    paginate(
      "Posts",
      "posts",
      GrdrBeam.post_list() |> Enum.to_list(),
      # TODO: Remove post_lists and use @file_lookup ETS
      # GrdrBeamWeb.list_files(),
      conn,
      params
    )
  end

  def tags(conn, params) do
    # TODO: Get basename in cache somehow
    paginate(
      "Tags",
      "posts",
      GrdrBeamWeb.tag_lookup(params["tag"]),
      conn,
      params
    )
  end

  def notes(conn, params) do
    paginate("Notes", "notes", GrdrBeam.note_list() |> Enum.to_list(), conn, params)
  end

  def slides(conn, params) do
    paginate("Talks", "talks", GrdrBeam.talk_list() |> Enum.to_list(), conn, params)
  end

  defp paginate(title, prefix, list, conn, params, num_per_page \\ 3) do
    # Grab page from params, default to 1
    page =
      with v when v != nil <- params["page"],
           {num, _} <- Integer.parse(v) do
        num
      else
        _ -> 1
      end

    max_page =
      list
      |> length()
      |> Integer.floor_div(num_per_page)

    # Don't go more than max_page
    page =
      max_page
      |> min(page)

    conn
    |> put_layout(html: :app)
    |> assign(:title, title)
    |> assign(:prefix, prefix)
    |> assign(:page, page)
    |> assign(:max_page, max_page)
    |> assign(
      :files,
      list
      |> Enum.sort_by(&(&1 |> elem(1) |> Map.get("date")))
      |> Enum.reverse()
      |> Enum.slice(((page - 1) * num_per_page)..(page * num_per_page - 1))
    )
    |> render(:posts)
  end

  defp file(conn, params, lookup: lookup, layout: layout, render: template) do
    # File name cache
    post =
      case lookup.(params["name"] <> ".md") do
        {:ok, filepath} ->
          # File content cache
          GrdrBeamWeb.build_or_get(
            &GrdrBeam.build_file/1,
            %{name: filepath, cache: params["cache"]}
          )
          |> Enum.map(fn {k, v} -> {String.to_atom(k), v} end)

        {:not_found} ->
          %{content: GrdrBeam.oops(), front_matter: :not_found}
      end

    conn
    |> put_layout(html: layout)
    |> assign(:content, post[:content])
    |> assign(:front_matter, post[:front_matter])
    |> render(template)
  end

  def post(conn, params) do
    file(conn, params, lookup: &GrdrBeamWeb.lookup/1, layout: :app, render: :post)
  end

  def note(conn, params) do
    file(conn, params, lookup: &GrdrBeamWeb.note_lookup/1, layout: :app, render: :post)
  end

  def slide(conn, params) do
    file(conn, params, lookup: &GrdrBeamWeb.talk_lookup/1, layout: :talk, render: :talk)
  end
end
