defmodule GrdrBeamWeb.PdfController do
  use GrdrBeamWeb, :controller

  def get(conn, params) do
    file(conn, params, "pdfs/")
  end

  def resume(conn, params) do
    file(conn, params, "resume/")
  end

  defp file(conn, params, dir) do
    IO.puts(params["name"])
    file_name = "content/static/" <> dir <> params["name"]

    case File.exists?(file_name) and String.ends_with?(file_name, ".pdf") do
      true ->
        conn = put_resp_content_type(conn, "application/pdf")

        send_file(conn, 200, file_name)

      false ->
        send_resp(conn, 404, "File not found")
    end
  end
end
