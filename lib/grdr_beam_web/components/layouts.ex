defmodule GrdrBeamWeb.Layouts do
  use GrdrBeamWeb, :html

  embed_templates "layouts/*"
end
